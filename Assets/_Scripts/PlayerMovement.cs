﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    float directionx;
    Rigidbody2D rb;
    bool facingRight;
    private Vector3 localScale;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        localScale = transform.localScale;
    }

    private void Update()
    {
        directionx = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(directionx * 10, 0);

        if (Input.GetButtonDown("Jump") && rb.velocity.y == 0)
        {
            rb.AddForce(Vector2.up * 7f);
        }
    }

    private void LateUpdate()
    {
        if(directionx > 0)
        {
            facingRight = true;
        }
        else if(directionx < 0)
        {
            facingRight = false;
        }

        if (((facingRight) && (localScale.x < 0)) || ((!facingRight) && (localScale.x > 0)))
        {
            localScale.x *= -1;

            transform.localScale = localScale;
        }
    }
}
