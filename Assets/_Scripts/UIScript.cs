﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class UIScript : MonoBehaviour
{
    public GameObject[] Menus;
    public TextMeshProUGUI CoinsCounterText;
    private string CurrentLevel;
    // 0 = MainMen, 1 = InGameMenu, 2 = GameOverMenu, 3 = GreyPanel
    private void Awake()
    {
        Menus[0].SetActive(false);
        Menus[1].SetActive(false);
        Menus[2].SetActive(false);
        Menus[3].SetActive(false);
    }
    public void OpenMenu() // Open the In Game Menu
    {
        Time.timeScale = 0;
        Menus[1].SetActive(true);
        Menus[3].SetActive(true);
    }

    public void OpenGameOverMenu() // Open the gameover menu
    {
        Menus[2].SetActive(true);
        Menus[3].SetActive(true);
    }

    public void ResumeGame() // Resume game button
    {
        Time.timeScale = 1;
        Menus[1].SetActive(false);
        Menus[3].SetActive(false);
    }

    public void StartGame() // start new game button
    {
        SceneManager.LoadScene("Level_1");
    }

    public void QuitGame() // finish the game button
    {
        Application.Quit();
    }

    public void RestartLevel() // restart the current level
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Level_1");
        Menus[2].SetActive(false);
        Menus[3].SetActive(false);
        
    }

    public void CoinsCounter(int Coins)
    {
        CoinsCounterText.text = "" + Coins;
    }


}
