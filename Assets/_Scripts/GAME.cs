﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GAME : MonoBehaviour
{
    public UIScript uiScript;
    public int Coins;

    private void Start()
    {
        GameEvents.current.OnPlayerEnterDeathZone += OnPlayerEnter;
        GameEvents.current.OnPlayerEnterExit += onPlayerExit;
    }

    private void onPlayerExit()
    {

    }

    private void OnPlayerEnter()
    {
        Debug.Log("I am being called");
        uiScript.OpenGameOverMenu();
        Time.timeScale = 0;
    }

    public void CoinCounter (int coinAdded)
    {
        Coins += coinAdded;
        uiScript.CoinsCounter(Coins);
    }


}
