﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public static GameEvents current;

    private void Awake()
    {
        current = this;
    }

    public event Action OnPlayerEnterDeathZone;
    public event Action OnPlayerEnterExit;

    public void PlayerEnterExit()
    {
        if(OnPlayerEnterExit != null)
        {
            OnPlayerEnterExit();
        }
    }
    public void PlayerEntersDeathZone()
    {
        if(OnPlayerEnterDeathZone != null)
        {
            OnPlayerEnterDeathZone();
        }
    }
}
