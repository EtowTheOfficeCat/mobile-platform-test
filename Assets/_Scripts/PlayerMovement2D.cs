﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerMovement2D : MonoBehaviour
{
    public CharacterController2D controller; // reference to charactercontroller script
    public Animator animator; // reference to the character animator

    public float runSpeed = 200f; 
    float horizontalMove = 0f;
    bool jump = false;



    private void Update()
    {
        horizontalMove = CrossPlatformInputManager.GetAxisRaw("Horizontal") * runSpeed; // directional movement of the player from 1, à to -1 on horizontal axis.

        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if (CrossPlatformInputManager.GetButtonDown("Jump"))
        {
            jump = true;
            animator.SetBool("IsJumping", true);
        }
    }
    public void OnLanding() // Animation stop when character landed
    {
        animator.SetBool("IsJumping", false);
    }

    private void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
        jump = false;
        
    }

}
